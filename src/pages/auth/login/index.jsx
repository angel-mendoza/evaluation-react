import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'

import {
  Grid,
  Card,
  Typography,
  CardContent
} from '@material-ui/core';
import LoginForm from '../../../components/form/LoginForm'

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center'
  },
  cardAction: {
    marginTop: '2rem',
    width: '100%'
  },
  action: {
    textDecoration: 'none',
    color: '#210000',
    fontWeight: 600
  },
  cardActionText: {
    fontSize: '1rem',
    fontWeight: 100
  }
}));

const Login = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container justify="center" item xs={12}>
        <Card>
          <CardContent>
            <LoginForm />
          </CardContent>
        </Card>
      </Grid>
      <Grid container justify="center" item xs={12}>
        <Card className={classes.cardAction}>
          <CardContent>
            <Typography variant="h6" className={classes.cardActionText}>
              ¿Aun no tienes una cuenta? Ingresa <Link className={classes.action} to="/register">aqui</Link>
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    </div>
  );
}
 
export default Login;
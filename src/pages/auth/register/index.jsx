import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'

import {
  Grid,
  Card,
  Typography,
  CardContent
} from '@material-ui/core';
import RegisterForm from '../../../components/form/RegisterForm'

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center'
  },
  cardAction: {
    marginTop: '2rem',
    width: '100%'
  },
  action: {
    textDecoration: 'none',
    color: '#210000',
    fontWeight: 600
  },
  cardActionText: {
    fontSize: '1rem',
    fontWeight: 100
  }
}));

const Register = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container justify="center" item xs={12}>
        <Card>
          <CardContent>
            <RegisterForm />
          </CardContent>
        </Card>
      </Grid>
      <Grid container justify="center" item xs={12}>
        <Card className={classes.cardAction}>
          <CardContent>
            <Typography variant="h6" className={classes.cardActionText}>
              ¿Ya tienes una cuenta? Ingresa <Link className={classes.action} to="/login">aqui</Link>
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    </div>
  );
}
 
export default Register;
import React from 'react';
import { connect } from "react-redux";
import { useSelector } from "react-redux";

const Home = () => {
  const name = useSelector(state => state.auth.information.name);
  return (
    <h1>Bienvenido {name}</h1>
  );
}
const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, null)(Home);
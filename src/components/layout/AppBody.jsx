import AppBar from './AppBar';
import { makeStyles } from '@material-ui/core/styles';

import {
  Grid,
  Container
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  body: {
    padding: '20px',
  },
}));

function App({children}) {
  const classes = useStyles();
  return (
    <div>
      <AppBar />
      <Container fixed>
        <Grid 
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={classes.body}>
          { children }
        </Grid>
      </Container>
    </div>
  );
}

export default App;

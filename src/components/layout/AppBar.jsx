import {
  AppBar,
  Button,
  Toolbar,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import { connect } from "react-redux";
import { autenticateUser, logout } from "../../store/actions/authenticateActions";
import { useDispatch, useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  },
  button: {
    textDecoration: 'none',
    color: '#fff'
  }
}));

const Navbarapp = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const loggedIn = useSelector(state => state.auth.loggedIn);
  let btn;
  if (loggedIn) {
    btn = <Button onClick={() => dispatch(logout())} color="inherit">Logout</Button>
  } else {
    btn = <Button color="inherit" component={Link} to="/login">login</Button>
  }
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          <Link className={classes.button} to="/">Evaluation</Link>
        </Typography>
        { btn }
      </Toolbar>
    </AppBar>
  );
}

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = { autenticateUser, logout };

export default connect(mapStateToProps, mapDispatchToProps)(Navbarapp);
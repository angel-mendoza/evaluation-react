import React from 'react';
import { connect } from "react-redux";
import Swal from 'sweetalert2';
import { makeStyles } from '@material-ui/core/styles';
import { autenticateUser } from "../../store/actions/authenticateActions";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { 
  Input,
  Button,
  InputLabel,
  IconButton,
  FormControl,
  InputAdornment,
  CircularProgress
} from '@material-ui/core';

import { Visibility, VisibilityOff } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  marginTop: {
    marginTop: '1.5rem'
  }
}));

const FormLogin = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [values, setValues] = React.useState({
    email: '',
    password: '',
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  
  const handleButtonClick = async () => {
    setLoading(true);
    const res = await dispatch(autenticateUser(values))
    if (res.type === 'success') {
      history.push("/profile");
    } else {
      Swal.fire(
        'Ups!',
        res.msj,
        'error'
      )
    }
    setLoading(false);
  };

  return (
    <div className={classes.root}>
      <div>
        <FormControl fullWidth className={classes.margin}>
          <InputLabel htmlFor="standard-adornment-email">Email</InputLabel>
          <Input
            id="standard-adornment-email"
            type="email"
            value={values.email}
            variant="outlined"
            onChange={handleChange('email')}
          />
        </FormControl>
        <FormControl fullWidth className={classes.margin}>
          <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
          <Input
            id="standard-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <FormControl fullWidth className={classes.margin}>
          <Button
            variant="outlined"
            size="large"
            color="primary"
            className={classes.marginTop}
            disabled={ loading }
            onClick={handleButtonClick}
          >
            { loading ? <CircularProgress size={24} /> : 'Ingresar' }
          </Button>
        </FormControl>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = { autenticateUser };

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);
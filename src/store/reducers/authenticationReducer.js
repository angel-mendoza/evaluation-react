import {
  LOGIN,
  LOGOUT,
  SET_LOADING
} from '../types/authenticateTypes';

const INITIAL_STATE = {
	information: {},
	loading: false,
	loggedIn: false
}

export default (state = INITIAL_STATE, {type, payload}) =>{
	switch (type){
		case SET_LOADING: 
			return { 
				...state, 
				loading: payload
			}			
      
    case LOGIN: 
			return { 
				...state, 
        information: payload,
        loggedIn: true
      }	
            
		case LOGOUT:
			return{
				...state,
				information: {},
				loggedIn: false,
			}
		
		default: return state 	
	}
}
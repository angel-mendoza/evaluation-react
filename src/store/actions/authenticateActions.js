import axios from 'axios';
import { LOGIN, LOGOUT } from '../types/authenticateTypes';

export const autenticateUser = (data) => async (dispatch) => {
  try {
    const response = await axios.post('http://104.236.0.42/api/login',data);
    dispatch({
      type: LOGIN,
      payload: response.dataUser
    })
    return response
  } catch (error) {
    return {
      type: 'error',
      type: 'Error al iniciar sesion',
    }
  }
}

export const registerUser = (data) => async (dispatch) => {
  try {
    const response = await axios.post('http://104.236.0.42/api/register',data);
    dispatch({
      type: LOGIN,
      payload: response.dataUser
    })
    return response
  } catch (error) {
    return {
      type: 'error',
      type: 'Error al iniciar sesion',
    }
  }
}

export const logout = (data) => async (dispatch) => {
  dispatch({
		type: LOGOUT
	})
}
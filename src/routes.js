import Home from './pages/home'
import Login from './pages/auth/login'
import Register from './pages/auth/register'
import Profile from './pages/profile'
export const routes_auth = [
  {
    path: '/profile',
    exact: true, 
    component: Profile,
  }
]

export const routes_guest = [
  {
    path: '/',
    exact: true, 
    component: Home,
  },
  {
    path: '/login',
    exact: true, 
    component: Login,
  },
  {
    path: '/register',
    exact: true, 
    component: Register,
  }
]
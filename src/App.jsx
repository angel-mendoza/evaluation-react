import {BrowserRouter, Switch, Route } from 'react-router-dom';
import { routes_auth, routes_guest } from './routes';
import AuthRoute from './components/router/AuthRouter';
import AppBody from './components/layout/AppBody';

function App() {
  return (
    <BrowserRouter>
      <AppBody>
        <Switch>
          {
            routes_auth.map((index , currentValue) => (
              <AuthRoute key={currentValue} exact={index.exact} path={index.path} component={index.component} />
            ))
          }
          {
            routes_guest.map((index , currentValue) => (
              <Route key={currentValue} exact={index.exact} path={index.path} component={index.component} />
            ))
          }  
        </Switch>
      </AppBody>
    </BrowserRouter>
  );
}

export default App;
